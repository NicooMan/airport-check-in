package main;
/**
 * Class made to read booking and flight files and to write the report file
 * 
 * @author Jean-Alexandre
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class IOHandler {
	private HashMap<String, Flight> flightMap;
	
	/**
	 * Creates a IOHandler object
	 */
	public IOHandler() {
		flightMap = new HashMap<String, Flight>();
	}
	
	/**
	 * Returns the flight HashMap
	 * @return the flight map
	 */
	public HashMap<String, Flight> getFlightMap() {
		return flightMap;
	}

	/**
	 * Sets the flight map instance
	 * @param flightMap the new flightMap
	 */
	public void setFlightMap(HashMap<String, Flight> flightMap) {
		this.flightMap = flightMap;
	}
	
	
	/**
	 * Reads both flight and booking files to completely fills the flightMap HashMap
	 */
	public void readAirlineFiles() {
		readFlight();
		readBooking();
	}
	
	/**
	 * Write the file "report.txt" using information in the parameter
	 * @param flightMap HashMap containing all the information about the passengers
	 */
	public static void writeReport(HashMap<String, Flight> flightMap) {
		FileWriter fw;
		
		try {
			fw = new FileWriter("./report.txt");
			System.out.println("Writing the report in the file \"report.txt\"...");
			
			for (Entry<String, Flight> entry : flightMap.entrySet()) {
				// reads every element in the flightMap HashMap and writes the flight key in the report file
				fw.write("Flight " + entry.getKey() + ":\n");
				// the next three variables are here to calculate totals
				double totalWeight = 0.0;
				int totalVolume = 0;
				double totalFees = 0.0;
				
				for (Booking booking : entry.getValue().getBookingList()) {
					// checks if each booking in the flight has checked in or not so it will writes only about the ones who did
					if (booking.isChecked()) {
						// write basic information about the passenger
						fw.write("\t" + booking.getBookingReference() + "\t");
						fw.write(booking.getName().getLastName().substring(0, 1).toUpperCase() + booking.getName().getLastName().substring(1).toLowerCase() + "\t");
						Baggage passengerBaggage = booking.getBaggage();
						if (passengerBaggage.getWeight() != 0) {
							// if this passenger has a baggage, writes info about it
							String dimensionsString = "";
							for (int i = 0; i < 3; i++) {
								if (i > 0) {
									dimensionsString += "x";
								}
								dimensionsString += passengerBaggage.getDimension()[i];
							}
							// add theses values to the total
							totalVolume += passengerBaggage.getVolume();
							totalFees += passengerBaggage.getPrice();
							totalWeight += passengerBaggage.getWeight();
							fw.write(dimensionsString + "cm");
							fw.write(" " + passengerBaggage.getWeight() + "kg" + " ");
							fw.write("£" + passengerBaggage.getPrice());
						}
						
						fw.write("\n");
					}
				}
				// write the different totals
				fw.write("\tTotal:\n");
				fw.write("\t\tVolume: " + totalVolume + "cm3 Weight: " + totalWeight + "kg Fees: £" + totalFees + "\n");
			}
			fw.close();
			System.out.println("Report written!");
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	/**
	 * Reads the flight file and fill the flight hashMap
	 */
	private void readFlight() {
		String flightFile = "./flight.csv";
		File readFlightFile = new File(flightFile);
		Scanner scanner;
		boolean start = true;
		int lineNb = 1;
		
		try {
			scanner = new Scanner(readFlightFile);
			while (scanner.hasNextLine()) {
				try {
					String flightLine = scanner.nextLine();
					
					// ignore the first line (column names)
					if (!start) {
						// check the lines and fill the HashMap
						checkFlightLine(flightLine, lineNb);
						lineNb += 1;
					}
					start = false;
				} catch (IllegalStateException e){
					System.out.println(e.getMessage());
				}
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			System.out.println("File" + flightFile + " not found.");
			System.exit(0);
		}
	}
	
	/**
	 * Check if line is perfectly formatted and if true, creates a new element in the flightMap HashMap
	 * @param line the line read
	 * @param lineNb the line number to know where the error happens
	 * @throws IllegalStateException error sent when the line has a format error
	 */
	private void checkFlightLine(String line, int lineNb) throws IllegalStateException {
		if (line.charAt(line.length()-1) == '\n') {
			line = line.substring(0, line.length() - 1);
		}
		String[] lineArray = line.trim().split(";");
		String flightCodePattern = "[A-Z]{2}\\d{4}";
		
		// right number of column in this line
		if (lineArray.length != 6) {
			throw new IllegalStateException("The line " + lineNb + " doesn't have the right amount of information.");
		}
		// checks the flight code (must be 2 upper case letters followed by 4 digits
		if (!lineArray[0].matches(flightCodePattern)) {
			throw new IllegalStateException("Wrong flight code format at line " + lineNb + ".");
		}
		// checks if this contains only char found in city names
		if (!lineArray[1].matches("[A-Za-z- ]+")) {
			throw new IllegalStateException("City name should only contain letters at line " + lineNb + ".");
		}
		// checks this is only digits
		if (!lineArray[3].matches("\\d+")) {
			throw new IllegalStateException("The max number of passengers must contain digits only at line " + lineNb + ".");
		}
		// checks if this can be turned into a float number
		if (!lineArray[4].matches("\\d+.?\\d+")) {
			throw new IllegalStateException("The max baggage weight must contain only digits at and/or a dot line " + lineNb + ".");
		}
		// check if this is only digits
		if (!lineArray[5].matches("\\d+")) {
			throw new IllegalStateException("The max volume of the flight must contain only digits at line " + lineNb + ".");
		}
		try {
			// put the info in the HashMap
			flightMap.put(lineArray[0], new Flight(lineArray[0], lineArray[2], lineArray[1], Integer.parseInt(lineArray[3]),
					Double.parseDouble(lineArray[4]), Integer.parseInt(lineArray[5])));
		} catch (NumberFormatException e) {
			throw new IllegalStateException("Max passenger or max baggage weight or max flight volume contains forbidden characters at line " + lineNb + ".");
		}
	}
	
	/**
	 * Read booking file to fill the ArrayList of Booking in each Flight object in flightMap HashMap 
	 */
	private void readBooking(){
    	try {
    		String filename ="booking_list.csv";
			File f = new File(filename);
			Scanner scanner = new Scanner(f);
			while (scanner.hasNextLine()) {
				// read first line and process it
				String inputLine = scanner.nextLine();
				if (inputLine.length() != 0) {// ignored if blank line
					try {
						processLineBooking(inputLine);
					} 
					catch (StringIndexOutOfBoundsException siobe) {
						System.out.println("Error in the data! Please enter correct data!");
					}
				}
			}
			scanner.close();
		}
		// if the file is not found, stop with system exit
		catch (FileNotFoundException fnf) {
			String filename ="booking_list.csv";
			System.out.println(filename + " not found ");
			System.exit(0);
		}

		catch (StringIndexOutOfBoundsException siobe) {
			System.out.println("Error in the archer data! Please enter correct data!");
			// System.exit(0);
		}
		
	}
	
	/**
	 * Reads each line of booking file to check and gather information needed in the Booking ArrayLists
	 * @param inputLine
	 */
	private void processLineBooking(String inputLine) {
		//Split the ligne by ;
		String separated[] = inputLine.split(";");
		// check if the flight in our flight list
		if (flightMap.get(separated[2])!=null) {
			String bookingCodePattern = "[A-Z]{1}\\d{3}[A-Z]{1}\\d{1}";
			if (!separated[0].matches(bookingCodePattern)) {
				throw new IllegalStateException("Wrong booking code format at line " + inputLine + ".");
			}
			if (!separated[1].matches("[A-Za-z- ]+")) {
				throw new IllegalStateException("Name should only contains letters at line " + inputLine + ".");
			}
			Name nameBooking = new Name (separated[1]);
			boolean checkedBooking = Boolean.parseBoolean(separated[3]);
			Booking bookedBooking = new Booking(nameBooking,separated[0],checkedBooking);
			flightMap.get(separated[2]).getBookingList().add(bookedBooking);
		}
		else {
			// line ignored
		}	
	}
}
