package main;

/**
 * The Class Booking.
 */
public class Booking {

	private Name name;
	private String bookingReference;
	private boolean checked;
	private Baggage baggage;

	/**
	 * Instantiates a new booking.
	 *
	 * @param name the name of the customer
	 * @param BookingReference the booking reference of the customer
	 * @param Checked the checked status
	 */
	public Booking(Name name, String BookingReference, boolean Checked) {
		this.name = name;
		this.bookingReference = BookingReference;
		this.checked = Checked;
		this.baggage = new Baggage();
	}

	/**
	 * Gets the baggage.
	 *
	 * @return the baggage
	 */
	public Baggage getBaggage() {
		return baggage;
	}

	/**
	 * Sets the baggage.
	 *
	 * @param baggage the new baggage
	 */
	public void setBaggage(Baggage baggage) {
		this.baggage = baggage;
	}

	/**
	 * Gets the name.
	 *
	 * @return name
	 */
	public Name getName() {
		return name;
	}

	/**
	 * Gets the booking reference.
	 *
	 * @return the booking reference number
	 */
	public String getBookingReference() {
		return bookingReference;
	}

	/**
	 * Checks if is checked.
	 *
	 * @return a boolean to know if it has been checked
	 */
	public boolean isChecked() {
		return checked;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(Name name) {
		this.name = name;
	}

	/**
	 * Sets the booking reference.
	 *
	 * @param BookingReference the new booking reference
	 */
	public void setBookingReference(String BookingReference) {
		this.bookingReference = BookingReference;
	}

	/**
	 * Sets the checked.
	 *
	 * @param Checked the new checked
	 */
	public void setChecked(boolean Checked) {
		this.checked = Checked;
	}

	/**
	 * To string.
	 *
	 * @return a sentence
	 */
	public String toString() {
		return name.getFirstAndLastName() + " " + bookingReference + " " + ((checked) ? "true" : "false");
	}
}
