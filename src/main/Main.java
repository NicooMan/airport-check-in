package main;


public class Main {
    public static void main(String args[]) {
        System.out.println("Hello Airport Check-In!");
        IOHandler myIOHandler = new IOHandler();
        myIOHandler.readAirlineFiles();
        Kiosk myKiosk = new Kiosk(myIOHandler.getFlightMap());
        CheckInGUI CheckInGUI = new CheckInGUI(myKiosk);
        CheckInGUI.setVisible(true);
    }
    
}