package main;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Kiosk class checking name/booking and baggage validity, and checking-in a passenger
 * 
 * 
 * @author Nicolas NGUYEN-KHOA-MAN
 */

public class Kiosk {

    private HashMap<String, Flight> flightMap;


    public Kiosk(HashMap<String, Flight> flightMap) {
    	this.flightMap = flightMap;
    }

    public HashMap<String, Flight> getFlightMap(){
    	return flightMap;
    }
    
	/** Check-in a passenger if he is not already checked-in and if his info and baggage are valid.
	 * @param lastName
	 * @param bookinRef
	 * @return a boolean
	 */
    public boolean check(String lastName, String bookingRef) {
    	Booking booking = findBooking(bookingRef);

    	if (booking.isChecked()){
    		return false;
    	}
    	else if (!booking.isChecked()) {
    		if (isValidName(lastName, bookingRef) && isValidBaggage(lastName, bookingRef)) {
    			booking.setChecked(true);
    			return true;
    		}
    	}
    	return false;
    }
    
    
	/** Check if the last name and booking ref match
	 * @param lastName
	 * @param bookinRef
	 * @return a boolean
	 */
    public boolean isValidName(String lastName, String bookingRef) {
    	Booking booking = findBooking(bookingRef);
    	if (booking != null) {
        	if (bookingRef.equals(booking.getBookingReference()) && lastName.toLowerCase().equals(booking.getName().getLastName().toLowerCase())) {
    			return true;
    		}
    	}
    	return false;
    }
    
	/** Check if the baggage corresponding to a booking fits in the plane, 
	 * i.e. if its weight and volume are lower than the remaining weight and volume available in the flight
	 * @param lastName
	 * @param bookinRef
	 * @return a boolean
	 */
    public boolean isValidBaggage(String lastName, String bookingRef){
    	Booking booking = findBooking(bookingRef);
    	Baggage baggage = booking.getBaggage();
    	Flight flight = findFlight(booking);
    	
    	int[] dim = baggage.getDimension();
    	double weight = baggage.getWeight();
    	int volume = dim[0]*dim[1]*dim[2];
    	if (volume < getRemainingVolume(flight) && (weight < getRemainingWeight(flight))) {
    		return true;
    	}
    	return false;
    }
    
	/** Find a booking
	 * @param bookingReference
	 * @return the booking
	 */
    public Booking findBooking(String bookingReference) {
    	for (Flight flight : flightMap.values()) {
    		for (Booking booking : flight.getBookingList()) {
    			if (booking.getBookingReference().equals(bookingReference)) {
    				return booking;
    			}
    		}
    	}
    	return null;
    }
    
	/** Find a flight
	 * @param booking
	 * @return the flight
	 */
    public Flight findFlight(Booking booking){
    	for (Flight flight : flightMap.values()) {
    		for (Booking b : flight.getBookingList()) {
    			if (b.getBookingReference().equals(booking.getBookingReference())) {
    				return flight;
    			}
    		}
    	}
    	return null;
    }
    
	/** Get the remaining volume available in a flight
	 * @param flight
	 * @return the remaining volume
	 */
    public int getRemainingVolume(Flight flight) {
    	ArrayList<Booking> bookings = flight.getBookingList();
    	int maxVolume = flight.getMaxVolume();
    	int sumVol = 0;
    	for (Booking booking : bookings){
    		if (booking.isChecked()) {
        		int[] dim = booking.getBaggage().getDimension();
        		int volume = dim[0]*dim[1]*dim[2];
        		sumVol += volume;
    		}
    	}
    	return (maxVolume-sumVol);
    }
    
	/** Get the remaining weight available in a flight
	 * @param flight
	 * @return the remaining weight
	 */
    public double getRemainingWeight(Flight flight) {
    	ArrayList<Booking> bookings = flight.getBookingList();
    	double maxWeight = flight.getMaxWeight();
    	int sumWeight = 0;
    	for (Booking booking : bookings){
    		if (booking.isChecked()) {
        		double weight = booking.getBaggage().getWeight();
        		sumWeight += weight;
    		}
    	}
    	return (maxWeight - sumWeight);
    }
}