package main;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * The GUI class.
 */
public class CheckInGUI extends JFrame implements ActionListener {

	private JTextField lastName;
	private JTextField bookingRef;
	private JLabel errorField;
	private JLabel errorFieldIn;
	private JLabel errorBaggageField;
	private JButton confirm;
	private JButton checkIn;
	private JButton calculateFee;
	private JTextField weightField;
	private JTextField[] dimensionField;
	private JTextField[] feeField;
	private JCheckBox checkBox;
	private Kiosk myKiosk;
	private Baggage myBaggage = new Baggage();

	/**
	 * Setup center panel.
	 *
	 * @param kiosk the kiosk
	 * @throws HeadlessException the exception
	 */

	public CheckInGUI(Kiosk kiosk) throws HeadlessException {
		this.myKiosk = kiosk;

		this.setTitle("Airport Check-In");
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				endProgram();
			}
		});

		this.setExtendedState(JFrame.NORMAL);
		this.setSize(1500, 1500);

		// The layout
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(9, 2));
		centerPanel.setBorder(BorderFactory.createEmptyBorder(10, 50, 10, 50));
		this.add(centerPanel, BorderLayout.CENTER);

		centerPanel.add(new JLabel("Last Name"));
		lastName = new JTextField();
		centerPanel.add(lastName);

		centerPanel.add(new JLabel("Booking Reference"));
		bookingRef = new JTextField();
		centerPanel.add(bookingRef);

		// Adding error field
		errorField = new JLabel("");
		// changing message color
		errorField.setForeground(Color.RED);
		centerPanel.add(errorField);

		// Adding confirm Button
		confirm = new JButton("Confirm");
		centerPanel.add(confirm);
		// Adding Action listener
		confirm.addActionListener(this);

		centerPanel.add(new JLabel("Add Baggage"));
		checkBox = new JCheckBox("");
		centerPanel.add(checkBox);
		checkBox.addActionListener(this);
		checkBox.setEnabled(false);

		JLabel dimensions = new JLabel("Dimensions (cm)");
		centerPanel.add(dimensions);
		JPanel dimensionsPanel = new JPanel();
		dimensionField = new JTextField[3];
		// Adding multi-fields
		for (int i = 0; i < 3; i++) {
			dimensionField[i] = new JTextField(2);
			dimensionsPanel.add((dimensionField[i]));
		}
		dimensionField[0].setEditable(false);
		dimensionField[1].setEditable(false);
		dimensionField[2].setEditable(false);
		centerPanel.add(dimensionsPanel);

		centerPanel.add(new JLabel("Weight (kg)"));
		weightField = new JTextField();
		weightField.setEditable(false);
		centerPanel.add(weightField);

		// Adding error field
		errorBaggageField = new JLabel("");
		errorBaggageField.setForeground(Color.RED);
		centerPanel.add(errorBaggageField);

		// Adding Calculate Fee
		calculateFee = new JButton("Calculate Fee");
		centerPanel.add(calculateFee);
		calculateFee.setEnabled(false);
		// Adding Action listener
		calculateFee.addActionListener(this);

		JLabel fee = new JLabel("Fee");
		centerPanel.add(fee);
		JPanel feePanel = new JPanel();
		feeField = new JTextField[2];
		// Adding multi-fields
		for (int i = 0; i < 2; i++) {
			feeField[i] = new JTextField(4);
			feeField[1] = new JTextField("�", 2);
			feeField[1].setEditable(false);
			feeField[0].setEditable(false);
			feePanel.add((feeField[i]));
		}
		centerPanel.add(feePanel);

		// Adding error field
		errorFieldIn = new JLabel("");
		errorFieldIn.setForeground(Color.RED);
		centerPanel.add(errorFieldIn);

		// Adding Search Button
		checkIn = new JButton("Check In");
		centerPanel.add(checkIn);
		// Adding Action listener
		checkIn.addActionListener(this);
		checkIn.setEnabled(false);

		this.add(centerPanel, BorderLayout.CENTER);

		this.pack();
	}

	/**
	 * Action performed, the action to execute when pushing a button.
	 *
	 * @param e the event
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

		/**
		 * The confirm Button will check if the person associated with the booking
		 * reference exist in the database. It will enable or disable other fields
		 * accordingly to the result of the Action.
		 */
		if (e.getSource() == confirm) {

			if (myKiosk.isValidName(lastName.getText().trim(), bookingRef.getText().toUpperCase())) {
				myBaggage = new Baggage();
				checkIn.setEnabled(true);
				errorField.setForeground(Color.GREEN);
				errorField.setText("Confirmed");
				// Modifying fields
				checkBox.setEnabled(true);
				lastName.setEnabled(false);
				bookingRef.setEnabled(false);
				confirm.setEnabled(false);
				errorFieldIn.setText("");

			} else {
				errorField.setForeground(Color.RED);
				errorField.setText("Error");
				// Modifying fields
				checkIn.setEnabled(false);
				checkBox.setEnabled(false);
			}
		}

		/**
		 * The check Box will activate fields for baggage informations if checked. It
		 * will enable or disable other fields accordingly to the result of the Action.
		 */
		if (e.getSource() == checkBox) {
			if (checkBox.isSelected()) {
				// Modifying fields
				weightField.setEditable(true);
				calculateFee.setEnabled(true);
				dimensionField[0].setEditable(true);
				dimensionField[1].setEditable(true);
				dimensionField[2].setEditable(true);
				checkIn.setEnabled(false);

			} else {
				// Modifying fields
				calculateFee.setEnabled(false);
				feeField[0].setText("");
				feeField[0].setEditable(false);
				weightField.setText("");
				weightField.setEditable(false);
				dimensionField[0].setText("");
				dimensionField[0].setEditable(false);
				dimensionField[1].setText("");
				dimensionField[1].setEditable(false);
				dimensionField[2].setText("");
				dimensionField[2].setEditable(false);
				checkIn.setEnabled(true);
			}
		}
		/**
		 * The calculate Fee button will calculate the fees that the passenger have to
		 * pay for the baggage they have. it is available only if the passenger have a
		 * baggage. It will enable or disable other fields accordingly to the result of
		 * the Action.
		 */
		if (e.getSource() == calculateFee) {
			try {

				// Setting baggage informations
				myBaggage.setWeight(Double.parseDouble(weightField.getText()));
				int[] dimensions = { Integer.parseInt(dimensionField[0].getText()),
						Integer.parseInt(dimensionField[1].getText()), Integer.parseInt(dimensionField[2].getText()) };
				myBaggage.setDimension(dimensions);

				if (myKiosk.isValidBaggage(lastName.getText().trim(), bookingRef.getText().toUpperCase())) {

					// Modifying fields
					errorBaggageField.setForeground(Color.GREEN);
					errorBaggageField.setText("Confirmed");
					float price = myBaggage.calculatePrice();
					feeField[0].setText(Float.toString(price));
					checkIn.setEnabled(true);
					feeField[0].setEditable(false);
					weightField.setEditable(false);
					dimensionField[0].setEditable(false);
					dimensionField[1].setEditable(false);
					dimensionField[2].setEditable(false);
					calculateFee.setEnabled(false);
					checkBox.setEnabled(false);
				}

				else {
					errorBaggageField.setForeground(Color.RED);
					errorBaggageField.setText("Max Weight");
					// Modifying fields
					checkIn.setEnabled(false);
				}
			} catch (NumberFormatException g) {
				// Modifying fields
				errorBaggageField.setForeground(Color.RED);
				errorBaggageField.setText("Please enter valid numbers");
				feeField[0].setText("");
				checkIn.setEnabled(false);
			}
		}

		/**
		 * The check In button check that the person hasn't already checked in before
		 * doing so. It will enable or disable other fields accordingly to the result of
		 * the Action so it can checkIn more passengers.
		 */
		if (e.getSource() == checkIn) {

			if (myKiosk.check(lastName.getText().trim(), bookingRef.getText().toUpperCase()) == true) {
				// Modifying fields
				errorFieldIn.setForeground(Color.GREEN);
				errorFieldIn.setText("Checked");
				myKiosk.findBooking(bookingRef.getText().toUpperCase()).setBaggage(myBaggage);
				lastName.setText("");
				bookingRef.setText("");
				feeField[0].setText("");
				weightField.setText("");
				dimensionField[0].setText("");
				dimensionField[1].setText("");
				dimensionField[2].setText("");
				errorField.setText("");
				errorBaggageField.setText("");
				checkIn.setEnabled(false);
				lastName.setEnabled(true);
				bookingRef.setEnabled(true);
				confirm.setEnabled(true);
				checkBox.setSelected(false);
				checkBox.setEnabled(false);
			}

			else if (myKiosk.check(lastName.getText().trim(), bookingRef.getText().toUpperCase()) == false) {
				// Modifying fields
				errorFieldIn.setForeground(Color.ORANGE);
				errorFieldIn.setText("Already Checked In");
				confirm.setEnabled(true);
				checkIn.setEnabled(false);
				lastName.setEnabled(true);
				bookingRef.setEnabled(true);
				checkBox.setSelected(false);
				checkBox.setEnabled(false);
				calculateFee.setEnabled(false);
				feeField[0].setText("");
				feeField[0].setEditable(false);
				weightField.setText("");
				weightField.setEditable(false);
				dimensionField[0].setText("");
				dimensionField[0].setEditable(false);
				dimensionField[1].setText("");
				dimensionField[1].setEditable(false);
				dimensionField[2].setText("");
				dimensionField[2].setEditable(false);
				errorField.setText("");
				errorBaggageField.setText("");
			}
		}
	}

	/**
	 * End program, Ask IOHandler to write the Report.
	 */
	private void endProgram() {
		IOHandler.writeReport(myKiosk.getFlightMap());
		System.exit(0);
	}
}
