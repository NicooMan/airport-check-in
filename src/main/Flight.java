package main;
import java.util.ArrayList;

public class Flight {

	//instance of all variable
	private String flightNumber;
	private String carieer;
	private String destination;
	private int maxPassenger;
	private double maxWeight;
	private int maxVolume;
	private ArrayList<Booking> bookingList;
	
	/** Create a new flight using all info
	 * @param flightNumber
	 * @param carieer
	 * @param destination
	 * @param maxPassenger
	 * @param maxWeight
	 * @param maxVolume
	 */
	public Flight(String flightNumber, String carieer, String destination, int maxPassenger, double maxWeight,
			int maxVolume) {
		this.flightNumber = flightNumber;
		this.carieer = carieer;
		this.destination = destination;
		this.maxPassenger = maxPassenger;
		this.maxWeight = maxWeight;
		this.maxVolume = maxVolume;
		this.bookingList = new ArrayList<Booking>();
	}
	
	/** get the flight number
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}
	/** set the flight number
	 * @param flightNumber the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	/** get the carieer of the flight
	 * @return the carieer
	 */
	public String getCarieer() {
		return carieer;
	}
	/** Set the Carieer of a flight
	 * @param carieer the carieer to set
	 */
	public void setCarieer(String carieer) {
		this.carieer = carieer;
	}
	/**Get the destination of the flight
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}
	/** set the destination of a plane
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}
	/** get the max passenger a plane can take
	 * @return the maxPassenger
	 */
	public int getMaxPassenger() {
		return maxPassenger;
	}
	/** Set the max passenger a plane can get
	 * @param maxPassenger the maxPassenger to set
	 */
	public void setMaxPassenger(int maxPassenger) {
		this.maxPassenger = maxPassenger;
	}
	/**return the max weight of a plane
	 * @return the maxWeight
	 */
	public double getMaxWeight() {
		return maxWeight;
	}
	/** Set the max Weight of a plane 
	 * @param maxWeight the maxWeight to set
	 */
	public void setMaxWeight(double maxWeight) {
		this.maxWeight = maxWeight;
	}
	/**Return the max volume of a flight
	 * @return the maxVolume
	 */
	public int getMaxVolume() {
		return maxVolume;
	}
	/**set MaxVolume of a flight
	 * @param maxVolume the maxVolume to set
	 */
	public void setMaxVolume(int maxVolume) {
		this.maxVolume = maxVolume;
	}
	/**Return an array with all the booking list
	 * @return the bookingList
	 */
	public ArrayList<Booking> getBookingList() {
		return bookingList;
	}
	/**
	 * @param bookingList the bookingList to set
	 */
	public void setBookingList(ArrayList<Booking> bookingList) {
		this.bookingList = bookingList;
	}
	
	public boolean doesPassengerExist(String lastName, String bookingCode) {
		for (Booking booking : bookingList) {
			if (booking.getName().getLastName() == lastName && booking.getBookingReference() == bookingCode) {
				return true;
			}
		}
		return false;
	}
	
	public String toString() {
		return "Carieer = " + carieer + ", destination = " + destination + ", maxPassenger = " + maxPassenger + ", maxWeight = " + maxWeight +
				", maxVolume = " + maxVolume; 
	}

}
