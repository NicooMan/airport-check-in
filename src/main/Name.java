package main;
/**
 * Name class with all the names of a person
 * 
 * @author Jean-Alexandre Ibanez
 */
public class Name {
	private String firstName;
	private String middleName;
	private String lastName;

	/**
	 * Creates a Name object with the first and the last names only
	 * 
	 * @param fName the first name
	 * @param lName the last name
	 */
	public Name(String fName, String lName) {
		firstName = fName;
		middleName = "";
		lastName = lName;
	}

	/**
	 * Creates a Name with the first, the last and the middle names
	 * 
	 * @param fName the first name
	 * @param mName the middle name
	 * @param lName the last name
	 */
	public Name(String fName, String mName, String lName) {
		firstName = fName;
		middleName = mName;
		lastName = lName;
	}

	/**
	 * Creates a Name with the full name
	 * 
	 * @param fullName the full name (with names separated with spaces)
	 */
	public Name(String fullName) {
		int spacePos1 = fullName.indexOf(' ');
		int spacePos2 = fullName.lastIndexOf(' ');

		firstName = fullName.substring(0, spacePos1);
		if (spacePos1 == spacePos2)
			middleName = "";
		else
			middleName = fullName.substring(spacePos1 + 1, spacePos2);
		lastName = fullName.substring(spacePos2 + 1);
	}

	/**
	 * Returns the first name
	 * 
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Returns the last name
	 * 
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Change the last name with value in parameter
	 * 
	 * @param ln the new last name
	 */
	public void setLastName(String ln) {
		lastName = ln;
	}

	/**
	 * Returns the first and last names separated by a space
	 * 
	 * @return the first and last names separated by a space
	 */
	public String getFirstAndLastName() {
		return firstName + " " + lastName;
	}

	/**
	 * Returns the last and the first names separated by a comma and a space
	 * 
	 * @return the last and the first names separated by a comma and a space
	 */
	public String getLastCommaFirst() {
		return lastName + ", " + firstName;
	}

	/**
	 * Returns either the first name and the last name or first name, middle name
	 * and last name separated by spaces
	 * 
	 * @return the full name with the right format
	 */
	public String getFullName() {
		String result = firstName + " ";

		if (!middleName.equals("")) {
			result += middleName + " ";
		}
		result += lastName;
		return result;
	}

	/**
	 * Returns the initials of the person (middle name included only if it exists)
	 * 
	 * @return the initials
	 */
	public String getInitials() {
		String initials = "";

		if (!firstName.equals("")) {
			initials += firstName.charAt(0);
		}
		if (!middleName.equals("")) {
			initials += middleName.charAt(0);
		}
		if (!lastName.equals("")) {
			initials += lastName.charAt(0);
		}
		return initials;
	}
}