package tests;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;

import main.Flight;
import main.IOHandler;
import main.Kiosk;
import main.Booking;

public class TestKiosk {


	@Test
	public void testIsValidName() {
        IOHandler myIOHandler = new IOHandler();
        myIOHandler.readAirlineFiles();
        Kiosk myKiosk = new Kiosk(myIOHandler.getFlightMap());
		String lastName1 ="Auzimour"; //Valid booking and last name
		String bookingRef1 = "Z268M9";
		String lastName2 ="Auzimour"; 
		String bookingRef2 = "ZZZ233";//Invalid booking
		assertTrue(myKiosk.isValidName(lastName1,bookingRef1));
		assertFalse(myKiosk.isValidName(lastName2,bookingRef2));
	}
	
	@Test
	public void testIsValidBaggage() {
        IOHandler myIOHandler = new IOHandler();
        myIOHandler.readAirlineFiles();
        Kiosk myKiosk = new Kiosk(myIOHandler.getFlightMap());
		String lastName1 ="//Invalid booking"; 
		String bookingRef1 = "Z268M9"; 
		Booking booking1 = myKiosk.findBooking(bookingRef1); 
		booking1.getBaggage().setWeight(100); //Set valid baggage
		
		String lastName2 ="DELON";
		String bookingRef2 = "M689N1";
		Booking booking2 = myKiosk.findBooking(bookingRef2);
		booking2.getBaggage().setWeight(50000000); //Set baggage weight greater than the maximum capacity
		assertTrue(myKiosk.isValidBaggage(lastName1, bookingRef1));
		assertFalse(myKiosk.isValidBaggage(lastName2, bookingRef2));
	}
}
